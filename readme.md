# Description #

[Point.im](http://point.im/) Crossposter automatically posts your new WP posts to the [Point.im](http://point.im/) microblogging service.

[XMPP Enabled](http://wordpress.org/extend/plugins/xmpp-enabled/) plugin is required

# Plugin info #

Contributors: [sandfox](https://github.com/sandfox-im), [skobkin](https://bitbucket.org/skobkin)

Tags: microblogging, point.im, jabber

Requires at least: 3.0

Tested up to: 3.9

Stable tag: trunk

Crosspost your Wordpress posts to [Point.im](http://point.im/)

# Installation #

1. Install the [XMPP Enabled](http://wordpress.org/extend/plugins/xmpp-enabled/) plugin
1. Upload `point-xp` folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

# Frequently Asked Questions #

Still no questions :) **[Ask one](https://bitbucket.org/skobkin/wordpress_point_xp/issues)**

# Changelog #

## 0.5 ##
HTML to Markdown conversion added

## 0.4 ##
Forked and rewrited for [Point.im](http://point.im/)

## 0.3.1 ##
* changed westland-avs.info and sand-fox.com to sandfox.org
* some refactoring

## 0.3 ##
* first public release
